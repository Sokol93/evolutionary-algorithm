#pragma once

#include "EvoException.h"

namespace evo
{
	// Exception class for invalid argument
	class InvalidArgument : public EvoException
	{
	public:
		// Default exception name with description
		InvalidArgument(const std::string &exception_description = "", const std::string &exception_place = "")
			: EvoException("Invalid argument", exception_description, exception_place) {}

		// Exception set with both name and description
		InvalidArgument(const std::string &exception_name, const std::string &exception_description, const std::string &exception_place)
			: EvoException(exception_name, exception_description, exception_place) {}
	};
}