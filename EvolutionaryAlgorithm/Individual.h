#pragma once

#include <memory>
#include "DefaultValueException.h"

namespace evo
{
	// Abstract class for modeling Individual in evolutionary algorithm
	class Individual
	{
	public:
		// Copies instance of Individual using 'operator ='
		// Arguments:	indiv - original instance of Individual to copy from
		Individual(const Individual &indiv) 
		{
			*this = indiv;
		}

		// Gets fitness value
		virtual double get_fitness() const
		{
			if (_fitness < 0)
				throw DefaultValueException("Default fitness", "Default fitness value get with get_fitness function");

			return _fitness;
		}

		// Returns new Individual as mutated instance of parent
		virtual std::unique_ptr<Individual> mutation() const = 0;
		// Returns new Individual as crossover instance of parents (first parent is the one calling)
		// Arguments:	second_parent - second parent to crossover
		virtual std::unique_ptr<Individual> crossover(const Individual& second_parent) const = 0;
		// Makes Individual with random attributes
		virtual void make_random() = 0;
		
		virtual std::unique_ptr<Individual> clone() const = 0;
		// Copies instance of Individual
		// Arguments:	indiv - original instance of Individual to copy from
		virtual Individual& operator = (const Individual &indiv) = 0;
	protected:
		// Fitness of individual 
		double _fitness = -1;
	private:

	};
}
