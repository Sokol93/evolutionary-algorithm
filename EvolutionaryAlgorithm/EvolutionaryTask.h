#pragma once

#include "Individual.h"

namespace evo
{
	// Abstract class defining task to be solved by evolutionary algorithm
	class EvolutionaryTask
	{
	public:
		// Function solving evolutionary algorithm task using specified Individual
		// Arguments:	individual - information required to solve evolutionary task
		virtual void solve_task(std::unique_ptr<Individual> &individual) = 0;
	};
}