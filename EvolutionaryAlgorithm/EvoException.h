#pragma once

#include <string>

namespace evo
{
	// General exception class for 'evo' namespace
	class EvoException
	{
	public:
		// Initializing with exception name and description
		EvoException(const std::string &exception_name = "", const std::string &exception_description = "", const std::string &exception_place = "")
			: _name(exception_name), _description(exception_description), _place(exception_place) {}

		// Gets exception message 
		virtual std::string print_msg() const
		{
			return print_name() + " " + print_description() + " " + print_place();
		}

		// Gets exception name
		const std::string& get_name() const { return _name; }
		// Gets exception description
		const std::string& get_description() const { return _description; }
		// Gets exception occurrence place
		const std::string& get_place() const { return _place; }
	protected:
		// Name printing template
		virtual const std::string& print_name() const { return "Exception '" + _name + "'"; }
		// Name printing template
		virtual const std::string& print_description() const { return "Message '" + _description + "'"; }
		// Name printing template
		virtual const std::string& print_place() const { return "Occurrence '" + _place + "'"; }
	private:
		// Exception name
		std::string _name;
		// Exception message text
		std::string _description;
		// Exception occurrence place
		std::string _place;
	};
}