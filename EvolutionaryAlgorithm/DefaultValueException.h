#pragma once

#include "EvoException.h"

namespace evo
{
	// Exception for information about default value of variable being used
	class DefaultValueException : public EvoException
	{
	public:
		// Default exception name with description
		DefaultValueException(const std::string &exception_description = "", const std::string &exception_place = "")
			: EvoException("Default value", exception_description, exception_place) {}

		// Exception set with both name and description
		DefaultValueException(const std::string &exception_name, const std::string &exception_description, const std::string &exception_place)
			: EvoException(exception_name, exception_description, exception_place) {}
	};
}