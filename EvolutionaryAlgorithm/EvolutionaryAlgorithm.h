#pragma once

#include <memory>
#include <vector>
#include "Individual.h"
#include "InvalidArgument.h"
#include "EvolutionaryTask.h"

namespace evo
{
	// Class to manage evolutionary algorithm
	// Can be customized to different approaches
	class EvolutionaryAlgorithm
	{
	public:
		// Initializes primary attributes of Evolutionary Algorithm
		// Arguments:	population_size - size of the population in every generation
		//			    max_generations - maximum number of iterations of the evolutionary algorithm
		//				prototype - prototype model to create initial population
		//				task - task to solve by evolutionary algorithm
		EvolutionaryAlgorithm(short population_size, short max_generations, std::unique_ptr<Individual> prototype, std::unique_ptr<EvolutionaryTask> task)
			: _population_size(population_size), _max_generations(max_generations), _prototype(std::move(prototype)), _task(std::move(task))
		{
			_population.reserve(_population_size);
		}

		EvolutionaryAlgorithm() = delete;
		EvolutionaryAlgorithm(const EvolutionaryAlgorithm&) = delete;
		
		~EvolutionaryAlgorithm()
		{
			for (auto &indiv_ptr : _population)
			{
				indiv_ptr.reset();
			}
		}


		// Gets population size. Zero means not initialized
		short get_population_size() const noexcept { return _population_size; }
		// Sets population size
		void set_population_size(short population_size)
		{
			if (population_size <= 0)
				throw InvalidArgument("Population size must be greater then 0!", "set_population_size");
			_population_size = population_size;
		}
		// Gets max number of generations. Zero means not initialized
		short get_max_generations() const noexcept { return _max_generations; }
		// Sets max number of generations
		void set_max_generations(short max_generations)
		{
			if (max_generations <= 0)
				throw InvalidArgument("Max number of generations must be greater then 0!", "set_max_generations");
			_max_generations = max_generations;
		}
	protected:
		// Reference prototype to create new specific individuals
		unique_ptr<Individual> _prototype;
		// Task to solve by evolutionary algorithm
		unique_ptr<EvolutionaryTask> _task;
		// Population of individuals - actual generation
		std::vector<std::unique_ptr<Individual>> _population;

		// Generates first generation of individuals
		virtual void generate_first_generation()
		{
			for (short i = 0; i < _population_size; i++)
			{
				_population.push_back(_prototype->clone());
				_population[i]->make_random();
			}
		}
		// Evaluates fitness of each individual in population
		virtual void evaluate_fitness()
		{
			for (auto &individual_ptr : _population)
			{
				//individual_ptr->
			}
		}

	private:
		// Population size (number of individuals in population)
		short _population_size = 0;
		// Max number of generations
		short _max_generations = 0;
		// How many generations it took to find solution
		short _result_generations = 0;
	};
}

